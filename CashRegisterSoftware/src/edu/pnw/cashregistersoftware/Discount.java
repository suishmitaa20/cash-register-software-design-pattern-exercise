/**
 * @author Suishmitaa Srianand
 */

package edu.pnw.cashregistersoftware;

/**
 * This class describes the various discounts offered.
 *
 */
class Discount {
  /**
   * The option selected by the user.
   */
  private int discountOption;
  /**
   * The discount amount to be applied.
   */
  private double discount;

  public Discount(int discountOption) {
    this.discountOption = discountOption;
    this.discount = 0;
  }

  public int getDiscountOption() {
    return discountOption;
  }

  public void setDiscountOption(int discountOption) {
    this.discountOption = discountOption;
  }

  public double getDiscount() {
    return discount;
  }

  public void setDiscount(double discount) {
    this.discount = discount;
  }

  /**
   * Calculates the discount based on the option selected by the user.
   * 
   * @return discount amount
   */
  public double generateDiscountAmount() {
    if (this.discountOption == 1) {
      this.discount = 0.1;
    } else if (this.discountOption == 2) {
      this.discount = 0.2;
    } else {
      System.out.println("Discount not available.");
    }
    return this.discount;
  }
}
