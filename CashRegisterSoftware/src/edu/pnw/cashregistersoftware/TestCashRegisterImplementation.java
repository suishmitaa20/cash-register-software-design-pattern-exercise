/**
 * @author suishmitaa srianand
 */

package edu.pnw.cashregistersoftware;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class.
 *
 */
class TestCashRegisterImplementation {

  /**
   * Tests whether the correct discount amount is extracted when the discount option is selected.
   */
  @Test
  void testDiscount() {
    Discount d = new Discount(1); // Selecting option 1: 10% off
    assertEquals(0.1, d.generateDiscountAmount());

    d = new Discount(2); // Selecting option 2:20% off
    assertEquals(0.2, d.generateDiscountAmount());

    d = new Discount(3); // Selecting a non-existent option
    assertEquals(0, d.generateDiscountAmount());
  }

  /**
   * Tests whether the price of each item is calculated correctly after applying the discounts.
   */
  @Test
  void testSubTotal() {
    Item i = new Item(5.0);
    Discount d = new Discount(1);
    BillItem b = new BillItem(5);
    assertEquals(22.5, b.calculateSubTotal(i, d)); // Price of an item with 10% discount

    d = new Discount(4); // price of an item with no discount i.e invalid option
    assertEquals(25, b.calculateSubTotal(i, d));
  }

  /**
   * Tests whether the amount of all the items in the cart are calculated.
   */
  @Test
  void testTotal() {
    Item i = new Item(5.0);
    Discount d = new Discount(1);
    BillItem b = new BillItem(5);
    CashRegister cash = new CashRegister();

    b.calculateSubTotal(i, d); // Price of one item
    assertEquals(22.5, cash.calculateTotal(b));

    b.calculateSubTotal(i, d); // Price of two items
    assertEquals(45, cash.calculateTotal(b));
  }
}
