/**
 * @author suishmitaa srianand
 */

package edu.pnw.cashregistersoftware;

/**
 * This class maintains the sum total of the items in the cart.
 *
 */
class CashRegister {
  /**
   * Sum total of all items in the cart.
   */
  private double total;

  public CashRegister() {
    this.total = 0;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  /**
   * Calculates the total cost of the items in the cart.
   * 
   * @param b the final price of each BillItem
   * @return the sum total of the items in the cart
   */
  public double calculateTotal(BillItem b) {
    total = total + b.getSubTotal();
    return total;
  }
}
