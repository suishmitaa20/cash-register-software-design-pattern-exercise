/**
 * @author suishmitaa srianand
 */

package edu.pnw.cashregistersoftware;

/**
 * This class describes each entry in the bill relating to an item i.e. it contains each item's
 * details. On giving the quantity and the discount, this class holds the price of that item
 * (subtotal).
 */
class BillItem {
  /**
   * Quantity of the item to order.
   */
  private int quantity;
  /**
   * The total discounted price of the item.
   */
  private double subTotal;

  public BillItem(int quantity) {
    this.quantity = quantity;
    this.subTotal = 0;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public double getSubTotal() {
    return subTotal;
  }

  /**
   * Calculates the final price of each item after applying the discount.
   * 
   * @param item Instance of Item class that holds the price of that item
   * @param d Instance of Discount class that holds the discount to be applied based on the user's
   *        selection
   * @return The subTotal/the final price of the item including the discount
   */
  public double calculateSubTotal(Item item, Discount d) {
    subTotal =
        (item.getPrice() * quantity) - (item.getPrice() * quantity * d.generateDiscountAmount());
    return subTotal;
  }
}
