/**
 * @author Suishmitaa Srianand
 */

package edu.pnw.cashregistersoftware;

/**
 * This class contains the details of an Item like price.
 *
 */
class Item {
  /**
   * Describes the price of the item.
   */
  private double price;

  public Item(double price) {
    this.price = price;
  }

  public double getPrice() {
    return price;
  }
  
  /**
   * Updates the price of the item.
   * @param newPrice the new price.
   */
  public void updatePrice(double newPrice) {
    price = newPrice;
  }
}
