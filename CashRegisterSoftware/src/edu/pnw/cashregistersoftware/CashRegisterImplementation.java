/**
 * @author Suishmitaa Srianand
 */

package edu.pnw.cashregistersoftware;

import java.util.Scanner;

/**
 * This class implements the cash register software by getting the price of an item, the quantity
 * and the discount and displays the total of each item along with the bill total.
 *
 */
public class CashRegisterImplementation {
  /**
   * Main method that implements the Cash Register Software.
   * 
   * @param args Array of String
   */
  public static void main(String[] args) {
    CashRegister cash = new CashRegister();
    Scanner in = new Scanner(System.in);
    String choice = "";
    do {
      System.out.println("Unit Price ($):"); // Get price
      Item i = new Item(in.nextDouble());
      System.out.println("Quantity:"); // Get quantity
      BillItem b = new BillItem(in.nextInt());
      System.out.println("Discount:\n1. 10% off\n2. 20% off"); // Get discount
      Discount d = new Discount(in.nextInt());
      System.out.println("Subtotal: $" + String.format("%.2f", b.calculateSubTotal(i, d)));
      System.out.println("Total: $" + String.format("%.2f", cash.calculateTotal(b)));
      System.out.println("Press q to quit or any key to continue ");
      choice = in.next();
    } while (!choice.equals("q"));
    in.close();
  }
}
